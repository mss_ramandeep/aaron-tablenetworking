<?php
session_start();
$p = dirname(dirname(dirname(dirname(__FILE__)))) . "/wp-config.php";
include $p;
global $wpdb;
?>
<div id="form_wrap">
<script src='http://code.jquery.com/jquery-1.10.2.js'/></script>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

<script>
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        // alert( pattern.test(emailAddress) );
        return pattern.test(emailAddress);
    };
    jQuery.noConflict();
    jQuery(document).ready(function($) {



        $("#apnabutton").click(function() { //function for first form

            var name = $("#name").val();
            var email = $("#email").val();
            var password = $("#password").val();
            var passwordlength = $("#password").val().length;
            var cpassword = $("#cpassword").val();


            if (name != "" && email != "" && password != "" && cpassword !="") {

                var checkEmail = isValidEmailAddress(email);
                if (checkEmail == false) {
                    alert('Please Provide a Valid Email.');
                    return false;
                }
                if (passwordlength > 5) {
                    if (password == cpassword) {
                        $.ajax({
                            url: "<?php
echo plugins_url();
?>/events_scheduling/query.php",
                            type: "POST",
                            data: {
                                name: name,
                                email: email,
                                password: password,
                                action: 'insert'
                            },
                            dataType: "json",
                            success: function(response) {
                                //alert(response);
                                //alert(response);

                                if (response.success == false) {
                                    alert(response.message);
                                    return false;
                                } else {
                                    $('#form_header').hide();
                                    $('#form_header1').show();
                                }
                            }
                        });
                        return false;
                    } else {
                        alert("Password And Confirm Password Do Not matched");
                        return false;
                    }
                } else {
                    alert("Password Should be atleast 6 character long");
                    return false;
                }
            } else {

                alert('Please Fill All Fields.');
                return false;
            }

        });
        $(".ral").click(function() { //functrion for get radios value for paypal
            var plans = $("input:radio[name='plan']:checked").val();
            //alert(plans);
            $("#Pamoun").val(plans);
        });

        $("#selected").click(function() { //functrion for 2nd form
            var plan = $("input:radio[name='plan']:checked").val();
            if ($('[name="plan"]').is(':checked')) {
                $.ajax({
                    url: '<?php
echo plugins_url();
?>/events_scheduling/query.php',
                    type: "POST",
                    data: {
                        action: 'Subscribed',
                        value: plan
                    },
                    success: function(response) {
                        //alert(response);                    
                    }
                });
            } else {

                alert('Please Select Any Plan To Complete.');
                return false;
            }
        });
    });
</script>
 

<style>
.error{
color:red;
}

</style>
<?php
$new = $wpdb->get_results("select * from fees");
foreach ($new as $key => $value) {
    if ($value->id == 1) {
        $one = $value->fees;
    } elseif ($value->id == 2) {
        $month = $value->fees;
    } elseif ($value->id == 3) {
        $unlimit = $value->fees;
    }
}

$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id  = 'mss.shintusingh@gmail.com'; // Business email ID

?>
<div class="container">
   <main class="template-page content  twelve alpha units" itemprop="mainContentOfPage" role="main">
      <div class="post-entry post-entry-type-page post-entry-3303">
         <div class="entry-content-wrapper clearfix">
            <div class="av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-0  el_before_av_hr  avia-builder-el-first   av-inherit-size" style="padding-bottom:10px;font-size:40px;">
               <h1 style="padding-bottom:10px;font-size:40px;" itemprop="headline" class="av-special-heading-tag">Boss Sign Up / Login</h1>
               <div class="special-heading-border">
                  <div class="special-heading-inner-border"></div>
               </div>
            </div>
            <div class="hr hr-short hr-center  avia-builder-el-1  el_after_av_heading  el_before_av_one_half "><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
            <div class="flex_column av_one_half first styleIpt  avia-builder-el-2  el_after_av_hr  el_before_av_one_half  ">
            <div id='form_header' >
               <form data-avia-redirect="" data-avia-form-id="1" class="  avia-builder-el-3  avia-builder-el-no-sibling " method="post" action=""  >
                  <fieldset>
                     <p id="element_avia_first_name_1" class=" first_form  form_element form_fullwidth"><label for="avia_first_name_1"> Name <span title="required" >*</span></label> <input type="text" value="" id="name" class="text_input is_empty" name="name" required=""></p>
                     <p id="element_avia_e-mail_1" class=" first_form  form_element form_fullwidth"><label for="avia_e-mail_1">E-Mail <span title="required" >*</span></label> <input type="email" value="" id="email" class="text_input is_email" name="email" required=""></p>
                     <p id="element_avia_password_1" class=" first_form  form_element form_fullwidth"><label for="avia_password_1">Password <span title="required" >*</span></label> <input type="password" value="" id="password" class="text_input is_empty" name="password" required=""></p>
                     <p id="element_avia_confirm_password_1" class=" first_form  form_element form_fullwidth"><label for="avia_confirm_password_1">Confirm Password <span title="required" >*</span></label> <input type="password" value="" id="cpassword" class="text_input is_empty" name="cpassword" required="" ></p>
             <div id="msg"></div>
                     <p class="form_element "><input type="submit"   id="apnabutton" value="Submit"></p>
                  </fieldset>
               </form>
            </div>
            <div id='form_header1' style="display:none;" >
                    <div itemtype="https://schema.org/Table" itemscope="itemscope" class="avia-table main_color avia-pricing-table-container avia-table-1  avia-builder-el-3  el_after_av_hr  avia-builder-el-last ">
            <div class="pricing-table-wrap">
                <form method='post' action="<?php
echo $paypal_url;
?>">
            
                    <h2 style="font-size:24px;">Choose The Event Subscription Levels</h2>
                        <label>One Time Event Subscriptions<br><?php
echo $one . "$";
?></label><input type="radio" class="ral" name="plan" id="plan" value="<?php
echo $one;
?>"></td>
                             <input type="hidden" name="business" value="<?php
echo $paypal_id;
?>">
                               <input type="hidden" name="cmd" value="_xclick">
                                <input type="hidden" name="item_name" value="Table networking ">
                                <input type="hidden" name="item_number" value="1">
                                <input type="hidden" name="credits" value="510">
                                <input type="hidden" name="userid" value="1">
                                <input type="hidden" id="Pamoun" name="amount" value="<?php
echo $e_val;
?>">
                                <input type="hidden" name="no_shipping" value="1">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="handling" value="0">
                                <input type='hidden' name='return' value='<?php
echo get_site_url();
?>/return'>
                                <input type="hidden" name="notify_url" value="http://mastersoftwaretechnologies.com/ramandeep/ipn.php" />
                            <td><label>Monthly Event Subscriptions<br><?php
echo $month . "$";
?></label><input type="radio" class="ral" name="plan" id="plan2" value="<?php
echo $month;
?>"></td>
    
                            <td><label>Unlimited Event Subscriptions<br><?php
echo $unlimit . "$";
?></label><input type="radio" name="plan" id="plan3" class="ral" value="<?php
echo $unlimit;
?>"></td>
                        </tr>
                        <tr>
                            <td><input type="submit" name="selected"  id="selected" Value="Next"></td>
                        </tr>    
                </table>
        </form>
            </div>
</div>
        
            </div>
               <div class="ajaxresponse ajaxresponse_1 hidden" id="ajaxresponse_1"></div>
            </div>
           <?php
echo do_shortcode('[login_form]');
?>
        </div>
      </div>
   </main>
   <!-- close content main element --> <!-- section close by builder template -->        
</div>