<?php

get_header();


?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
 <script type="text/javascript">
              function initialize() {
              var address = (document.getElementById('location'));
              var autocomplete = new google.maps.places.Autocomplete(address);
              autocomplete.setTypes(['geocode']);
              google.maps.event.addListener(autocomplete, 'place_changed', function() {
                  var place = autocomplete.getPlace();
                  if (!place.geometry) {
                      return;
                  }

              var address = '';
              if (place.address_components) {
                  address = [
                      (place.address_components[0] && place.address_components[0].short_name || ''),
                      (place.address_components[1] && place.address_components[1].short_name || ''),
                      (place.address_components[2] && place.address_components[2].short_name || '')
                      ].join(' ');
              }
            });
      }
      function codeAddress() { 
          geocoder = new google.maps.Geocoder();
          var address = document.getElementById("location").value;
          geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                  if(results[0].types[0] == "country"){
                  //console.log(results[0].address_components[0].long_name);
                  $('#location').val(results[0].address_components[0].long_name);
                  console.log(results);

            }else{
                  $('#location').val('');
                  $('#responseDiv h4').html("Please select a valid country").css('color','red');
                  $('#responseDiv h4').css('display','block').delay(2000).slideUp("slow");
                  //console.log("not valid country");
            }
            
            } 

            else {
                  $('location').val('');
                  $('#responseDiv h4').html("Please select a valid country name").css('color','red');
                  $('#responseDiv h4').css('display','block').delay(2000).slideUp("slow");
            }
          });
        }
      
   </script>
<?php
global $wpdb;

if (isset($_POST['add_event'])) {
    $e_location      = $_POST['event_loc']; //quwery to add event in database
    $e_name          = $_POST['event_name'];
    $event_date      = $_POST['event_date'];
    $no_of_people    = $_POST['no_of_people'];
    $event_type      = $_POST['event_type'];
    $event_fees      = $_POST['event_fees'];
    $expectations    = $_POST['expectations'];
    $duration        = $_POST['duration'];
    $food_items      = $_POST['food_items'];
    $public_event    = $_POST['public_event'];
    $private_event   = $_POST['private_event'];
    $recurring_event = $_POST['recurring_event'];
    //echo "ok";
    $address         = $e_location; // Google HQ
    $prepAddr        = str_replace(' ', '+', $address);
    $geocode         = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false');
    $output          = json_decode($geocode);
    $latitude        = $output->results[0]->geometry->location->lat;
    $longitude       = $output->results[0]->geometry->location->lng;
    $wpdb->insert('Events_reg', array(
        'event_name' => $e_name,
        'event_location' => $e_location,
        'lat' => $latitude,
        'long' => $longitude,
        'event_date' => $event_date,
        'no_of_people' => $no_of_people,
        'event_fees' => $event_fees,
        'event_type' => $event_type,
        'expectations' => $expectations,
        'duration_of_the_event' => $duration,
        'food_items' => $food_items,
        'public_event' => $public_event,
        'private_event' => $private_event,
        'recurring_event' => $recurring_event
    ), array(
        '%s',
        '%s'
    )); //query to add data
    //query ended
    
    
    
}

?>
<div id="tab2" class="tab_content"> 

       <center><h3>Create Events</h3>
            <br />
        
           <!--form for create events-->
    <form id="ad" method="post" action="">
        <table>
        <tr><td><lable>Event Location</lable></td><td><input type ="text" name="event_loc" id="location" required="" placeholder="Event Location"></td></tr>
        <tr><td><lable>Event Name</lable></td><td><input type ="text" name="event_name" id="event_name" required="" placeholder="Event Name"></td></tr>
        <tr><td><lable>Event Date</lable></td><td><input type ="date" name="event_date" id="event_date" required="" placeholder="Event Date"></td></tr>
        <tr><td><lable>No Of People</lable></td><td><input type ="text" name="no_of_people" id="no_of_people" required="" placeholder="No Of People"></td></tr>
        <tr><td><lable>Event Fees</lable></td><td><input type ="text" name="event_fees" id="event_fees" required="" placeholder="Event Fees"></td></tr>
        <tr><td><lable>Event Type</lable></td><td><input type ="text" name="event_type" id="event_type" required="" placeholder="Free or Paid"></td></tr>
        <tr><td><label>Expectations</label></td><td><input type ="text" name="expectations" id="expectations" required="" placeholder="Expectations"></td></tr>    
        <tr><td><label>Duration of the event</label></td><td><input type ="text" name="duration" id="duration" required="" placeholder="Duration of the event"></td></tr>    
        <tr><td><label>Food Items</lable></td><td><input type ="text" name="food_items" id="food_items" required="" placeholder="Food Items"></td></tr>    
        <tr><td>Events</td><td><input type="checkbox" name="public_event" id="public_event" value="1" >Public Event
                <input type="checkbox" name="private_event" id="private_event" value="2" >Private Event
                <input type="checkbox" name="recurring_event" id="recurring_event" value="2" >Recurring Event</td></tr>
            <tr><td><input type="submit" name="add_event" value="Create Event" ></td></tr>
        </table>
    </form></center>
    </div>





<?php
get_footer();
?>