<?php
/*--------------------------------------------------------------------------------------------
|    @desc:         pagination 
|    @author:       Aravind Buddha
|    @url:          http://www.techumber.com
|    @date:         12 August 2012
|    @email         aravind@techumber.com
|    @license:      Free!, to Share,copy, distribute and transmit , 
|                   but i'll be glad if i my name listed in the credits'
---------------------------------------------------------------------------------------------*/
function paginate($reload, $page, $tpages) {
    $adjacents = 2;
    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";
    $out = "";
    // previous
    if ($page == 1) {
        $out.= "<span  style='float: left;list-style: outside none none;padding-left: 2%; text-decoration: none;'>" . $prevlabel . "</span>\n";
    } elseif ($page == 2) {
        $out.= "<li style='float: left;list-style: outside none none;padding-left: 2%; text-decoration: none;'><a  href=\"" . $reload . "\">" . $prevlabel . "</a>\n</li>";
    } else {
        //$out.= "<li style='float: left; padding-left: 2%;'><a  href=\"" . $reload . "&amp;pg=" . ($page) . "\">" . $prevlabel . "</a>\n</li>";
    }
  
    $pmin = ($page > $adjacents) ? ($page - $adjacents) : 1;
    $pmax = ($page < ($tpages - $adjacents)) ? ($page + $adjacents) : $tpages;
    for ($i = $pmin; $i <= $pmax; $i++) {
        if ($i == $page) {
            $out.= "<li   style=' float: left;list-style: outside none none;padding-left: 2%; text-decoration: none;' class=\"active\"><a href=''>" . $i . "</a></li>\n";
        } elseif ($i == 1) {
            $out.= "<li style='float: left;list-style: outside none none;padding-left: 2%; text-decoration: none;'><a  href=\"" . $reload . "\">" . $i . "</a>\n</li>";
        } else {
            $out.= "<li style='float: left;list-style: outside none none;padding-left: 2%; text-decoration: none;'><a  href=\"" . $reload . "&amp;pg=" . $i . "\">" . $i . "</a>\n</li>";
        }
    }
    
    if ($page < ($tpages - $adjacents)) {
        $out.= "<a style='font-size:11px' href=\"" . $reload . "&amp;pg=" . $tpages . "\">" . $tpages . "</a>\n";
    }
    // next
	//echo $page;
	if(!$page){$page=1;}
    if ($page < $tpages) {
        $out.= "<li style='float: left;list-style: outside none none;padding-left: 2%; text-decoration: none;'><a  href=\"" . $reload . "&amp;pg=" . ($page + 1) . "\">" . $nextlabel . "</a>\n</li>";
    } else {
        //$out.= "<span  style='float: left; padding-left: 2%; font-size:11px'>" . $nextlabel . "</span>\n";
    }
    $out.= "";
    return $out;
}
