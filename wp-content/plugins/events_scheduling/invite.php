<?php
$p = dirname(dirname(dirname(dirname(__FILE__)))) ."/wp-config.php";
 include $p;
global $wpdb ;
//-----------------------functions starts from here------
function getDistance($latitude1, $longitude1, //function to get distnce
$latitude2, $longitude2, $unit = 'Mi')
{
   $theta = $longitude1 - $longitude2;
   $distance = (sin(deg2rad($latitude1)) *
   sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) *
   cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
   $distance = acos($distance);
   $distance = rad2deg($distance);
   $distance = $distance * 60 * 1.1515;
   switch($unit)
   {
      case 'm': break;
      case 'K' : $distance = $distance *1.609344;
   }
   return (round($distance,2));
}                                              ////function to get distnce ended
function send_mail($user_email,$event_date,$event_title,$event_locations,$user_name){

            $to = "$user_email";
            $subject = "Notification Mail from Table Networking";
            
              $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Simples-Minimalistic Responsive Template</title>
     
   </head>
   <body>
<!-- Start of preheader -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" >
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of preheader -->       
<!-- Start of header -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
   <tbody>
      <tr>
         <td>&nbsp;</td>
      </tr>
   </tbody>
</table>
<!-- End of Header -->
<!-- Start of main-banner -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                           <tbody>
                              <tr>
                                 <!-- start of image -->
                                 <td align="left" st-image="banner-image">
                                    <div class="imgpop">
                                       <a target="_blank" href="#"><img width="150" border="0" alt="" style="display:block; border:none; outline:none; text-decoration:none;" src="data:image/gif;base64,R0lGODlhbgGqAMQAAAAAAObm5mZmZikpKbW1tZSUlBAQEEpKSv///8zMzHt7ezo6OqWlpff39yEhIQgICN7e3sXFxWtra1JSUoyMjBkZGe/v79bW1jMzM729vUJCQpmZmYSEhFpaWq2trXNzcyH5BAAHAP8ALAAAAABuAaoAAAX/ICKOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSCwaj8ikcslsOp/QqHRKrVqv2Kx2y+16v+CweEwum8/otHrNbrvf8Lh8Tq/b7/i8fs/v+/+AgYKDhIWGh4iJiouMjY6PkJGSk5SVlpeYmZqbnJoWAQ2doo4ZCgcHAgwBo6yGARIAsbIaGa22gBYdsrsAFbW3wHobvLwDFsHIdRYHxLwMydBxCQ7NuwrR2G0RBtWxDxzZ4Wnb3bEToeLpY+TlFRHq8GDs3QYe8fdc0+UABgn4/1j0lRMAsGCVCwPKSThm8MwnC+g2BVjQ7cE7Iw0saIyhMSKrAAQYMMjgcQaBAxUw/3BguOYhkwYC6EEwckHAAAcaPJREYaEAhgoCZooJAGpLBgzWdr7Y8GBXB5ZRLEAQGqOBhw4YJvxSQqBpMwdQg0x0xiKAhl0HwsJoQFRpEA8LBgjYSgMCAwoke1yoQOzZjL3ErkmJMGHAgg1qVwyTZYAuEg5eeRkgUIQBsQFUUXgg5thFgwgCMCxQ4K9y5AedPTNIGIuDWxqwiHV4rQIyMQy0j0CgJosCDAisZWFITMQCxWYEhzTQxcviiq68XHOUwE1WhQ1EgPM6N2PxrgI7IFTntWCVDAXNBlyIkoA88RMExsuirMTC2a8Rct8ATMyeCui7DGDeC7H1NUQEvMniQP9mnh0X4IA3EFCNA6XFUEBksqg3AwQeUKBTDt7FogGEK1gmm34+IEiPAAUQwKAOmzWD3X/EHEDiChDw1YwGKNaAEDEzwhBAcLI84B8OMRJjwEUxZCBfLA7cyMIGC3g1gZRCEglAeUulh+VbDpbzAAYfbPBiDQ2g1wx4Kpi1ywP0vRDfhBX+EIGORR75wm7NCIZDAhhaV+cLFiC1iwMf9ChCAxzU+F4LFmipoQsUNNNPcRlEEIECge4TywJnzmBBTM2As4IHkUUZgwdP7uKbWPfJssCjKCTQKgDc4VBoMxiEygIBeD5AAa0mRNApAEHSwACGs73QqJJM+hAABx9oUIH/Awl6SkxyODTwQTUC5MYfP4OywGo3GhBrw7myNBuDsTt+uRapvLg7wwSMxflCkryEa8OusfgCg5qSlasDA4Zq6+kDfuFgoqMrUMCsrydkgOe2is7QQKy4zjDPLlx2W+Au3MoQQMK4ZoxABsc6sF4N4jGWLAvfEvOAwTjcqfDOveZQQDUVvHxCAAK0OsAHOJtgAb7VNBzEs7FcGsMFt4KqA8cZCg1DAmGaI68JtlZDgcoigMTMLhUs0AEHHiRAW82ckb2CcTvv7NwNCVzMCwYKvEiBlrw4oIC6tu1IMQ4QYD3ABl+LYEEGBBQeeNJbHwsAmy4kQAFK1QywkgsZdCPd/wsRcFDYrd4YcK2pJ0BdDAYYYI7DnHVrezcNPWVbTVojeGs5eZQjQIDuvHzwgwUEdEA8Pw4I4MGX9lVgAOoA6DmD5ijvHTwCFlCAgQG/e+N543lbGi0LClQQvs3ni5BB1zZLoIPktZezpA0frC/LLw1IoL8sB3jN0qgHADjxgAASGAABo+aACThtBBfyFAasB4MNqG8fDjjMa5xUtwdU4IEm6F8zeJe55XmKdY6jjrYoNDsT1o8zNYjUzvwiobpNJgUewNYDdlgNe92AKfVzwPkAtrAJHA4+erNf+0YAoLoZbwUf+1TjRkC/nTmALg9bmOxEBb8Xiq0GLNvZNRpwNv+7+ckEF5qeBhKwgeUVoAFwzIjcvPXCCugLAQxYIC8UoDKseQqEI6hU/V6lgqWVSl0IYNoLh0PFupXsei70olMQaQKCaWs2VKsfI0+gCwwcoAMiEKRkJlAACmAlUTKgY/3sSIIGdLEchHQB7WaYAnrV7YwpEMh2jsgnSfoLAXDT1gF+qEdJYoYGZdTWOQC1yiUiQBc7HCYCIqCwLbZgLC+MpcRqB0gVMKduqUGAm+r3yFpWYwEFuMBrqlg/cIiQZ1NEARklqS2pcSSSxOBR6JqJgjzGIjk19JQ7YhC2F3Krl3WjoArGpTCX1Qqf1RhRWV4ZNQlk4EZDoqc33jEyTwn/sQY602g5HCoDdilsOFm0otZIoA8jWSABydzHpED3vxGiY5sddCYKClRTEpqAX3UbqGIUdgAKaM2S9OQRUvfxgJkNTKT7CBkMlhG1k/bkhQNwC7D4IYAJJDGqlARq3dKFgI0FcaUL1RE6iykBt4iynSUqpnAuElKRNvWt3UhJLOQ3g5NBtRzWbAF0DKCB/w0nRDubQAqC6UW+tuBnXmThj2rXMxeQyqHZqwYqT9DRsX7Jn7VT7DPpGRe0LNVSGGjKrLpTU0nqVAX+NIAD/sejgNbNsSbobP1upwKYSPKGdAstIjkoMASclhibze1vcRZBTR4jVhUA3D4YEMwJuK6e/wplwTf/ept4lqC5O1tAA9pTvzuSwJaS5BEL7CNJhoVSrrJA4Qpg4V4RiLUZyW2lbndGQVW+kIXfnIAAWvuA0OQLstH9n41ioB3uNiOWMPBndBVmgKnANxagTIEfvYjLEzTYi6ZqQEg68L8KbG8EzFCVCBirWaUEIKa1cypia3dDqBmmtQUkGQIEea3/HRMGqHIwMUyMu4RU4ANf/WfC8kMB8FE4nAj4sEYfEFiWXnhbJTguMXDLgphIrQGZRY5SlvOm2uGSiF7ETpBFKiA8klMGeBUyAHwYAy93IMmXI9WPI7CBYhJZM1dWkCK9YV4SUJOeohWBlAeiqGVVTwRR7P8hcbzjZDGV1hxQiRk9QWlWkdIHXnWTbwu0/FenwqArD5htNxZgqF8i4AL/M8CXSL2PAgRguwCo7AlSWr8ABnJn9lwvUl5Fa1lMgDgPc8ACDUABes301ThuBpdMCuIRvBgADhiA/q74OEqy2MEsjCFEnRKRC0D0AR0mwYbrOZNFc5kEca6dVAftKQivoFGzAfM+NJAQn5YAQPqDEzNjQZeBS/KjCEBv7XwIAQIEALx5PcBPruSCYiNabiSg9z5QiM1yqPehXvS18JqT3etqUii6FKZ3d/OAC0T6TR2gyFM0UztURAaFh6anUMu3SCzN2GYDwFM3TfBz7oq6pDX9saL/w8wLaaIAiC9E4XUlWkmNktW4teMtCxolcaZiBQBtfbpGF2xfkYYbAezUFm766VEBJGjmZRHABeU89Bf4d+MeWXQzKvClbytMX6Pihan3uzNuIQzHR0+BXz1lAAkgZXQl8C09MTAgyNLz7PNcZaiKrsQ9ZcADHEBJpTVqahns848lOL2YCr3iyK40AIOmeu9gHGMSPI7EthMAJRFA+ADBBQDdNOTlhWb59uqJgzRGqwgcXTt7t6ABU+n9zqocA9uWwzHFL8cTTwDx8KpFyg+U4W8pyF5PScC7IpB+LA5wgbNA2eT120r2qz0CnucUBRYPDP6gSn0YqL4bcOc4uLZq/ylQV9iiMK5mbQ5iJCUQAAkSaPXVOxKAZ7uQeCmAZqv2PgCgfPYVbTAUSlCVK9NEgd1QaJlXO9nlAgOYTTmQc+XweutGDCqmNPdhAAwQg7zgfPwSdrbHGhnET63EdBgTAxYkUHdmABwoTkL4dyPgd5RFFcjHTbsWaPdTA7RXP9tnA6CWVwyyeIwHZRHQARrwDBpXDRQ0TgCgdNbGG6JBTiXhSgrDgzBwhSMkLzj1QlWYSFB1dlFYN9YUXOWFJji4M2pIAwVFD68nXc3QVJZlO+3zMBFoe0hhAB2giN2gayOAUPuQbixQhtVQeta2hFE1IBRVN2c3S3VjbxAgitWAGv9oworVhAMMxSskklG2038jIH77EGzcEyv+RgLoMQCrUTcYkBqoCEszoHDd8FplRYcSZB4OCFW6dowKY2/2R4xHdIGWWDusZAN61y8lYYsLA4qG9j9alyR5WHUOkEPAxnrK+EUy4ISSkYRNKEkkBHUa5Wqctw/vNn8KQ2dCMm62k4Iu4IWLaF4GKSZ1NwLv2Ax6Io4WuBwG4EmtdXY9uIgPNgN3WA7PJnbNMAGdYgALUB1HAn8vlCwJsI3F0BQGIF/CRx7r43yEQoKM9wA2qCulKIcKqDCRiAIBQJOy4DT0cmwp0D8V4IxfYTDUiAG6RZAp0H2dc0RNxBgXAD+TwQD/CwB5nlg7JFV/HFBMeMEAERAW5rZlh1gNUFZxU6YBrFcVOFho5TeOW1c3cfIwbaYCwzAAsKggZ7JUwvJ/vXBiaGSOzDiCxHAO30YZmWEBArkPCTgCsEh91odtENAA2fIAGtABEldgGJeL+QeAOnCCNpMaVCUZSURlLDCI2+EPFhMw2yMxp1A3xnACMGZigPmLLgCVn8gCdSUL2DGZPVlWW0mIODOc8eVW+odmFPIJEBAB6JdLGjUAgrkCoikZS/SS7cIBevMN1LmXCsI5BeSUvWhKFyZeJiCOGbIyWyY3utknvKk3QcM92cOAJiCPRLVY2gJ5rYRerthpsTCbPcBr//WzkJ7xlicgeSQzWbuQhSfQm/VDjkxUARzQmOvXoMQzTGFUgTTQnrJBKw5KlAjqm7QpUleHRtrCRx6WPVS3XWvHA6VJTwxKA+hJDM53d/9UnegWMR7YG/oBe/xQP4lGAqClY1sYCxCaAhtZDlIFcq7CRMQgaproRRl2AvcVGEqRob1Rj8bmA9W5SNm4ArNYL4kRZ8aDVMKyAg2ZVw6AARuAIrhnUCfAYtwZRW25AnDzABJQABywpxxQNNZBjwiQcrcDiEZqAmlKYUN3li3WOh+4Y5HxbjiQkF5EoOayD5g4ArwGHvjIDwtpnGsCAfHUh7UTo3HJGLXwMekIA4rEd/8lgJ1piQBr9p8Q8lY9SW1e1JEkMKP4tRPYmYZUwXzcqReB1gyQGgNptwuFBpjPgIqNUUilWA24WAKH2gsH0CrBSgIKah2rcJZ/xhEOEqQjECK4iFf5ZXBg10rPqjAlGkLJlGpPkl8icI24whIZipo9AJhedKkycAEFYAqnIT2UKCVnaUApJ1QoUJZm1iO6eIkEkFlnWgLZ+imhgLD/CahMiixD4yDwWgIddUfYKXu2iocLOTKYqSU6KQIUe6MjwA4GcKRw9lcGlAOlKgAhQRwMRWRSdkMpQI2e8pi5pEdpAWP2ylLEk2GruAv6Klh44kxQg5u9s10V8CL8MlPqp0X/NLMdWHOy3DNo9UACGXqTLqqSobYDbyVyB5stGkKowSmkgSaCiqE/BNFZYOu1SSQYUpa0U1IdFQZospoCRwtANzIPOjta9JSjc2kN2eezwERuJAAgc6sDk7lpnVkCU0uPdysU7UqOBRBoThunJ4oAfcYLxUUCtlpfUmY1MhC6aYglrdkLSYiKRFkCUuYnHzCRQEmsuSl4jEkyieE6ash8j5sD8WaPz5kCWNqsfosyK/omMhlKHti5yuUpavYkdymkgaJiEBAmqFt91bGuuXocrogCeKW1aJgyKKsKqmkpr4qpgocAyaR7J4BYTkMwLdsD9ilcOjAPBjs0KBO7tvSw//j3vIhUtbUwpObAfbJhbRyzvVujI2ZrqEGZAh11dNh5qZ5qKZQKq7wAHtsVgI1bPOclMz1QtWK0A4fIwOeJMu4STEN7AtNqUxW3Piykut5wdCZHSJe7Iazhtt/lFVXWq87XcfxwPp/JvKAjH9gRTB68svLhU7FSvzxgkvSXA17Iw7KLMo5FPzFKAivoKVaMApFLHjNhwPs7Ap2FOSmLwi4AMAC5sryBokoTU6YmxBhLuh5ouCxgf+5lS0uMstkSn1GGJ8E7OzsKLjuAoFOKArMbrvKRyCaAlODSI95iLd3AHdS2IJz0pPUnH2rsAvfhyJF3HIp7bbugUKQsC1ymqP8Ks8UeliBwEo2yoLjZywv+EauDfAO7+1dtTAOitMsoezF+Qm1OdwLpSqwZEwCEoVnuczHVm3H65z4Y0sktwBygzLEY5mLtep3o5W8/CaeQ4iDuYH1ayz0xhUrB1LU9AMk7Q3Y5cHrVzFIY4iet+ynEsbA7s7EsMLwzMs9pqBb+KQvbZ32yFwPbVE6Uyw1Lmoko05VZtjeZ0QBi+5Ge8U0Ukn34HKJbsgocM7o70MX/ZbEvgLDvvLLxPAIpW4i5GNFWKgORi7wGjAEnQKixsH0pFbsyoDMGLYn8oHxTadPftR1hUcw14hn00rLXBccmcF1RawEXU8Y5cMEK0626wjT/vhyoJV02ROLUWP1CSL0nSTQp1KYB/ItcvzZJRWa+KnA2hYZUoPgx1gTVTecZlgQ70ZECiFVh1MbROqDOFFaYMFAz0DtNGIJCYXLLUabSe0Q2kkpCKVvNEQsAfnJdVb0CFLFJ8nQcoibEDN2AHANCJBzXLiCgTQrGgaI5GCLVOdClv+XXzlKxKvB/sYRrrJyyO6Ofa+xHjpWykJpysSAYaQKONTCJ9Ni6PHyNirsowRSjUlwOwxwxtZZLukMA+XMoxduJ3JWqNwA34ywC1uc09PbOtK0wFuisNMrI1lAxSRRijHXcLICwdSoQvHiNC8kvS7yP3dDcKvByspCCdEwB/9MtK5O7Uw5WpzFwygDQlln0bJziFGfLglVRKfJhsP/rfFuVg4vSUduNPl6xkE2msiadINKs0LNAImG8bz1icNOzC7xozbsgAR0eyz8wvF4UrYTyTchrAlLmtgwFrh/u4P4nkiizfWg2uKTbKjPyTqiMcXADoRFQANRAn5CGJ6xcAnCT0OQlb4j0Vg5wZ8b2Hi8qIh4QLC5reg425TIgfOjmFlfeGiawXfitaBSapU2yAekz4vX3x1ISsn5ho8XaAqrLiUyEAJAVvkgOAOwNQabagLeLtOqiIt6ATg7SvOoZOMMjKD8AyyG4e6/tFQsAZbB23v82HrMd52x+5vKBQv9pLL7NQB+/TTIY5+iWfQLqtDFNIU3jAsWtVBJZpJMYqC3NjAKWh5kccAH4SH231gyg4RWxfshwLUFf2lsRECsO0E0Q0FEPzD3BobjyKt5kg1fyZUus7AHVGjDZVhqt7g2H3lsFkiwQ8AEgBAEbUAC/UO0ToJmA1Kd5Zy3SA7+zF0RfQ0bXMgDn92oXI5OzrCCzFbWattk7sNzcCNIqAAEKIB9EHq828SYFphaDdnSOXju2zZtaskXjYoFlaUce8CHQbA4bAPEHixIOkCyHJgDTiQIxIV8QkAGQs1KF3lCH0wAXkABALwHPYEtGhAIu6AsJwAD+AMsWyQNFqlHrC6b/e7oB96HGdzoAFCAS0MhSwTGD1rYBe6oAfroLGvA3xEDjmahCBaRsA9AZzvZ9BcAazzAomgboU5UpJZHMLfnsNHjgLyDjeTWdoWM8odcBLu7lHiABHeBAIrABJGEVHOD4Aa54pN4NGdwChCHzJ5AABOABG7ABdF9UhranWf/v2YMdJpfTmlEB13IYEaD03JMALPFiqrPZIiSSrNOcuRh6B3D5NYAwaTjmHsb6EC/a7RD1ZfUT1W3SZ4HPx4PYJ8QDCUCzJkMNyO/CFq6oSEgoP58AJNJk+gISGeA2kRcSEQCFGACgizL5BUkBfHEA10+5AOC9Rm8/qqbiGRw6fd4C/2EOAB+w/CCAiCNZktYCqCurGkMbq4Jl2reZqFyD38nDEfD5Ni2BqBM7EJuIi8EBcdoaCsCD0qNyu5ePSjLtijgAifNSkakGEkPrUaAeHmOy6Gpg4PukxgQbS4XAmqBKR42fSUNGykIEGQPAwRZZQcvC0IVDDMUiQoMGAIFfxqhGJOgqSUSggxYZIMBGUwCG4MBGZ4uCk06HH8QBgEYCK16DxOHKBwwzDTJJgN5n1zJHH0VLRYbIxkOLwbEf5oefAhyHojSrBQMMhgcZxMA40SzbAwNxS3CTAADkyBDo9GFIOy5WmM0YBc1SwoIAJtxpguKBt0sxaoUK1GKCHygD2P9xSTAKg6qE0gJwMPDgwIUuGQAMqMjoiiAF/VgsaAIFJJ6WeyCqbGKG4YGAzCQQbQdhWQU+VEQi5GKk14gEhlYYKEXGAjGvXTis4VC1qLQLAQ1IOEvEyISmJCYJOpCiRU0iHx6kpBIg0IKYaK0yBLBgA5xDH+RKa+DBwQMBbnF4oMT4Bl0W/0SAacFEo68uJgGgvDzYT4MIo2CZJrEs9A0oghZoCDco4w0IBj5zyQDjA8nTRAgUHhBhK5vFwhFACDhALBEFD2A7qezZEidugqlEeICh9Z+WD9YtL+p4wIMBHsCD3YcjgEMZGCbY5ir1hnToTQo8GFreSQSFVTDaIUz/ldcAYgYoAF4DB+zBBQH1qaABSUpxpZ8PFmhgAG5NmPTAAgP9p5IFFEB2QAStXWBPX3/gJMMACiCHBUc2BFDBAuCJEIAAIIo4Ig6yMVPBBS+yEc1/F9BXGhGcOLAdEQkktkIlJGjFggE1NrENdRlS0B8PQJ5mwQdwCAClDRFiYNMIM7FhQAHPrJAFDhy4ByAGkQUnpo28HIKBBVcJ8qSY4pHnQwYg6miBnyrMUYKFKpzzSzFUjBYin8tdUMd4k5GASQdy3fImPDFkY4MFAwzAIAP9PZqpD6MyE5ebh1gDZASP/GjCNgbisxMA3ZiQwZSGsafBgBbxpyCs/3mQAgYM/+w5QkC3/tFZDAZ4gMsRN2Ci5Q0BLONAh82iGl9OT0gowwTTLpcOABS42xEAr+Kw0IWM3KWCA7uaYIa1sQFWrrmDNVBAJxO0OIIopNiAiQwPELDvCkgwMkAFnlqZwgEaFyyCBUowMw8EM8ZAIawZ4HKAvztC1jIJ1rkwjwkQzxwliERYUIBLCsz7sUoB7HUGmiKoQaQJmcWRALAqdCnCJAEnHc7TQF/LEB8lM3PAz8IpQyNj3Y3UZH3aViFnlj40sMA9OCQQiHpWi6kaAAbIa0JlOZZAHBseiMzCqSVgYACbI5AZ7H1yP8zQOQHIyUa7BXsAwwRFiwAqEY5zRbMJgv8CELgN24DeuZkeKz5YqQtwPsIVk47QHRsa1NHC6AhU5noOqMB8Ot+HhJpPXV2X1xziNygh9Qke1b36CX6iEZt3jF2ghAOJnw5kABTAMUG5gayenT6mmjBKywysseD1QZr8UQNfH5IX0Ax00pYJFuBCMMPYmn3DMirwVsLaBHKDXQBAAIVL338SoBQxjMBxSANZ01iwLgB0aSabKYEerIfAaaBLBhpon5Fk0DagTY8mBAOCEHBgszvZQGaAsoEZqlbCqGywYBkgBmtEMBO9lYENDljAuroUiIUNwzC7Q6D7ZlMDmwliYeY62APGQxS4MKZ39cKB1oJ1Qu/siYAdsFz/DcX0DlxE63JnyEqjuFKsGVjpJUQhwG9MV0MmymABNZCZIJgHtASkQANoqtYNYOeoe+3kgiIYRbkgoISo6CiM5QkAfygRiWXUiI6H0AAJAlIu7T1gA41MX6IuWYNQHgJcVqNGsOylIYnZ4EpsxMGLHiABdsSwBPIDQAcO6MhmfaFuHxiGAVQRgTXmQjAXcKPhAoKSXfqgHqJEgJSYEQtQwqBjI4AC4UywonwNcBDk6I4mGiiB/kiLmadLDTHckJ4hQIBbDOkkZwDAuQTgQjLmxIEFlOfBGrhSEMpBoLgAUL0RTIJrJXDmCpaZJglFA5EjgONEdHnPJ8LjAAQ4lQeI/4mFPLUADVAIJwLMl4VPhvEokOsB+AQRqho2AI4P6ABCroC+rMwId61YIwWMEBoLpKMCnpzoBgMQgQ1wQADIetMEZsQEM/DBAuMcAP6A+jqNIqIHEEhjDOzoSB4FgWZKSBwetXiDCLEAAxTA0RBU9oDKSXWDDYgUlorlgDWmYgDfuUCeKNJWfOrTH1Z9nAxSuMvHFNACqRqhSaMYl26y4ACDSwCZXEXSvWIvqfWJ4gII4IEJHGACCiCsZsxXAGIZIEyUxYxKe6A5ZhxRcQHoQBAiwAkMDKEBf+uAAqomgv6toAMd2AA9KdHa0zarAQkowAcE8AEFbGBPXzpCAyAQgf8BOABDxB1BO9mwUg0xRI9hvGUB7JQNVwaTALq9agtqcYUKIO+69wyr/0JxXIleF64rMFASBWEvZhbxAAVIwKYSioAN2JRaEowWJjDl3r0OUwafIEsHOLCBDFwgAJN1ZHYdPALPsWGmE80tBBDGApAQwJQ74wYHIkCAaS64rR+ogIReypliVUADEigAARJgYeJuIzmvY4gALiy3UUQBAwvogBMREDIJGiByLSYuBDLAAAoo4AMf2ABCGsEABUxgcIM4MgU8oGMhFyyl2VJFALDKvrY2gAE5hsAWLBCByfBIALnNqQfk+OQ9ByABW+5ysQyAAdx6YM7M5PASQPa32JH/2WrRjQADONCBvApvz5bugnT/7GWuzEcBDDA0AkMog4woTT6N5tOjpyyAA9gjDki6NKwbA4EEeKCoR+W0ZycMZ6tBgGKQE0EW2TA2gF6gxJLWQKsHMZ8PFKDQlY41tPEQgGIXQAK3dsEAbIxj+gqn1G8iB7YCq+cR9dkD1XYsZLhxZObmeMfRfvcjL5CBDVgbOVFYgAAoQAAwDiZXE4yBKngqiAdmqtwUsDbG1uWApHDg0xU+NbwjvogGTHuoHzhAo+6dbwLsWjj9ZAMP8xuHqKok1UVdQMKxNIAJSEDfCYCAuyUuc6A1wAKZ5sAE0MOVH0ZYzM9WCG99SA4r/FsF/6wsigWo/YEOLGCuEsJRhDew7wBYAOIzv/pyLFBuSXPUBS84wJUj8HMckLWUDy36CtpLBoNLgNUVcAkLBnCAlhcaAoa1Otbzjuqkr3jSU+rPwsMecyosmRlSySeQ8WBzSEt6AW+HO7bBjuOH413vlqd5dAlQAKOiJ4pRXLgEJnyB9qmtVVgDWV/PjI8+l3jpGICx56M4gHWLvuqVvzzugwrpD+TcJbaBepgvwA45F6ADVG3BrRosCF+FouZCVTWy/96flTP7zbfPPfYnagE/68QexVoTAuKEdhlc0PRsKEUA5q0AvzO5AtTX9dizL/9oG1fTFSCSFfWxRqCMQHt1rKtBpBiA+9FeBnTc/B0gAoaCbCGAr7XAwlEAA+TcAoDdwoRbxfQAB8jdB4SZjiWgB35gCRQAyj1eBTiAXQmAiNgdDiCaYShC/IEgDOJesREADb7cZAXdCgxbDO4gD67CW4kQv/WgEA6hDagBGwQhESbhEL6Wqsye7HRAAVyfEk6h3iXdBVwAzFUdFW4hF3ahF34hGIahGI4hGZahGZ4hGqahGq4hG7ahGzpBCAAAOw==" class="banner"></a>
                                    </div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <!-- end of image -->
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of main-banner --> 
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <tbody>
                  <tr>
                     <td align="center" height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->   
<!-- Start Full Text -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- Title -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 20px; color: #333333; text-align:left; line-height: 30px;" st-title="fulltext-heading">
                                                Dear '.$user_name.',
                                             </td>
                                          </tr>
                                          <!-- End of Title -->
                                          <!-- spacing -->
                                         
                                          <!-- End of spacing -->
                                          <!-- content -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:left; line-height: 20px;" st-content="fulltext-content">
                                               <p> Thanks for being a member of Table Networking! We are trying to learn more about how our members use our site, and you have been selected to take a short 8-minute survey that will help us improve our site.</p>

                                               <p> you have subscribed  with email <b> '.$user_email.' </b></p>
                                               

<!-- <p>Your answers will be completely anonymous and combined with other members responses.</p> -->

<!-- <p>To take part, please respond in the next two business days. We will close the survey once we receive the desired number of qualified responses.</p> -->



<p>We appreciate your time!</p>

<p>Best Regards,<br/>
Table Networking Team</p>
                                             </td>
                                          </tr>
                                          <!-- End of content -->
                                       </tbody>
                                    </table>
<table>
            <tr>
            <th>Event Name &nbsp;&nbsp;</th>
            <th>Event Date &nbsp;&nbsp;</th>
            <th>Event Location &nbsp;&nbsp;</th>
            </tr>
            <tr>
            <td>'.$event_title.'</td>
            <td>'.$event_date.'</td>
            <td>'.$event_locations.'</td>
            </tr>
            </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->
                           
                             <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of full text -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>&nbsp;</td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->   
<!-- 3 Start of Columns --><!-- end of 3 Columns -->
<!-- Start of seperator --><!-- End of seperator --> 
<!-- 2columns --><!-- end of 2 columns -->
<!-- Start of seperator --><!-- End of seperator -->   
<!-- Start Full Text --><!-- end of full text -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <tbody>
               
                 <tr>
                     <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->  
<!-- Start of Postfooter -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter" >
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <tr>
                                 <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;color: #666666" st-content="postfooter">
                                    Click here to <a href="#" style="text-decoration: none; color: #0a8cce">Unsubscribe</a> 
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of postfooter -->
   
   </body>
   </html>';
            
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            
            // More headers
            $headers .= 'From: <webmaster@example.com>' . "\r\n";
            $headers .= 'Cc: myboss@example.com' . "\r\n";
            
            if(mail($to,$subject,$message,$headers))
            {
              return $msg='1';
            }else{
               return $msg='0';
            }
} 

//---------------------------functions ended

//echo count($subscribiers); 
 $event_id = $_POST['sixtymile']; //.......................function for 60 miles invite---------------
if($event_id)
{ 
 $result = $wpdb->get_results ( "SELECT * FROM Events_reg where id= '$event_id'" );//query to get record of event to invite subscribers
  $rows = count($result);
  if($rows > 0)
  {
    foreach($result as $val){
        $event_locations = $val->event_location;
        $longitude1 = $val->e_long;
        $latitude1 = $val->e_lat;
        $event_title=$val->event_name;
        $event_date=$val->event_date;
    }
    //echo $event_long ;
    $subscribiers = $wpdb->get_results ( "SELECT * FROM Users where User_type = '4'" );//query to get all subscriber users in array
print_r($subscribiers);
   foreach($subscribiers as $subs)
   {
     $longitude2=$subs->u_long;
     $latitude2=$subs->u_lat;
       $d = getDistance($latitude1,$longitude1,$latitude2,$longitude2);
       if($d <= '60')
       {
        //echo $d;
                $user_email=$subs->Email;
                $user_name =$subs->Name;
        //---------------mail to users for invite
           $msg=send_mail($user_email,$event_date,$event_title,$event_locations,$user_name);
            
       }
       
   }
         if($msg)
            {
              echo "Mail Sent To Users";
            }else{
              echo "Some Error Occured";
            }
  }else{
    echo "False";
  }
  
}
  $before_attend_user_id=$_POST['for_before_attended_user']; //section for users who  attended events before
 if(isset($before_attend_user_id))
 {
   $box1_id=$_POST['for_before_attended_user'];
 $results = $wpdb->get_results ( "SELECT * FROM Events_reg where id= '$box1_id'" );//query to get record of event to invite subscribers
  $rowss = count($results);
  if($rowss > 0)
  {
    foreach($results as $valss){
          $event_locations = $valss->event_location;
          $event_title=$valss->event_name;
         $event_date=$valss->event_date;
    }
      
  }
  
  //$mms=send_mail($user_email,$event_date,$event_title,$event_locations);
  
  $q=$wpdb->get_results("select json,id,status from tables");
 //echo "<pre>";  print_r($q);
      foreach($q as $a)
      {
        $z=json_decode($a->json,true);
        //echo "<pre>";
        $b_email[]=$z['custom'];
      }
       $le=count($b_email);
      $event_locations;
      $event_title;
      $event_date;
      for($i=0;$i<$le;$i++)
      {
          $user_email=$b_email[$i];
          $msg = send_mail($user_email,$event_date,$event_title,$event_locations);
          
      }
       if($msg)
       {
         echo "Mail Sent To Users";
       }else{
         echo "Some Error Occured";
       }
      
 }      //section attended events ended
 
 
 $p_email=$_POST['puser_email'];//..................function for personal invite--------
if($p_email)
{
 
 $box_id=$_POST['box_id'];
 $results = $wpdb->get_results ( "SELECT * FROM Events_reg where id= '$box_id'" );//query to get record of event to invite subscribers
  $rowss = count($results);
  if($rowss > 0)
  {
    foreach($results as $valss){
         $event_locations = $valss->event_location;
         $event_title=$valss->event_name;
         $event_date=$valss->event_date;
    }
      
  }
   $user_email=$p_email;
   $msg=send_mail($user_email,$event_date,$event_title,$event_locations);
   if($msg)
       {
         echo "Mail Sent To User";
       }else{
         echo "Some Error Occured";
       }
}
?>
