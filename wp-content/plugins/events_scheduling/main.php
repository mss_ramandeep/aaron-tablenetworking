<?php

    /*
    Plugin Name:Event Scheduling Plugin
    Plugin URI: http://www.mastersoftwaresolutions.com/
    Description: Plugin for Events scheduling with diffrent types of user login functionality.
    Author: Ramandeep Singh
    Version: 1.0
    Author URI: http://www.mastersoftwaresolutions.com/
    */

    	//hook to create tables on plugin activations function starts
?>

<?php
    register_activation_hook( __FILE__, 'pages_plugin_install' );

    //register_deactivation_hook( __FILE__, 'pages_plugin_uninstall' );

    function pages_plugin_install() {
	include 'activate.php';
	}
	//function create table end here 
	
	//function to drop tables from database  on plugin deactivation
	function pages_plugin_uninstall() {
	include 'deactivate.php';
	}
	//function drop tables end here

add_action( 'admin_menu', 'pages_plugin' );

//register plugin with wordpress menu
function pages_plugin() {
	add_menu_page("Pages Settings","Event Scheduling",9,__FILE__,"Events"); 
	add_submenu_page(__FILE__,'Forms', 'Manage Users', 'manage_options', 'ram', 'for_admin');
	//add_submenu_page(__FILE__,'Forms', 'Manage Events', 'manage_options', 'raman', 'for_admin');
	add_submenu_page(__FILE__,'Forms', 'Super Admin Settings', 'manage_options', 'rsss', 'for_admin_fee');
	add_submenu_page(__FILE__,'Forms', 'Events', 'manage_options', 'parth', 'for_parth');

}

function forms_options() {//function for registration form

	include'action.php';	
}
add_shortcode('show_form','forms_options');//short code for registration form
function for_admin(){//function for user details in admin dashboard

	include 'users_details.php';
}
function for_parth(){//function for user details in admin dashboard

	include 'event_details.php';
}
function for_admin_fee(){//function manage_subscription fees in admin dashboard

	include 'subscription_fees_manage.php';
}
function add_login(){//function for login form on front end

	include 'login.php';
}
add_shortcode('login_form','add_login');//shortcode for login form
function dashboard(){
	include 'dashboard.php';
}
add_shortcode('dashboard_go','dashboard');//shortcode for dashboard
function register_session(){//register session
    if( !session_id() )
        session_start();
}
add_action('init','register_session');
function subscriber_reg(){
	include 'subscriber.php';
}
add_shortcode('subscriber_reg','subscriber_reg');//shortcode for subscriber signup



function events_regi(){
include 'events.php';

}
add_shortcode('events_regi' ,'events_regi');//shortcode for subscriber after his login.
function events_create(){
 include 'createevent.php';
}
add_shortcode('events_create','events_create');

function eve_create(){
 include 'manage_event.php';
}
add_shortcode('manage_events','eve_create');

function Subscribe_create(){
 include 'manage_subscriber.php';
}
add_shortcode('manage_subs','Subscribe_create');

function bussiness_create(){
 include 'bussiness_card.php';
}
add_shortcode('buss_create','bussiness_create');

function news_create(){
 include 'news_letter.php';
}
add_shortcode('new_create','news_create');

function boss_menu(){
 include 'boss_menu.php';
}
add_shortcode('boss_m','boss_menu');

function eve_t_admin(){
 include 'manage_event_admin.php';
}
add_shortcode('event-ad','eve_t_admin');
function event_public_display(){
 include 'events_public_display.php';
}
add_shortcode('event-display','event_public_display');
function event_admin_dashboard(){
 include 'eve_ad_dashboard.php';
}
add_shortcode('event_admin','event_admin_dashboard');
function event_sub(){
 include 'eve_sub.php';
}
add_shortcode('event_subs','event_sub');

?>



