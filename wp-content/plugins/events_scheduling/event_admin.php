<main class="template-page content  nine units" itemprop="mainContentOfPage" role="main">
   <div class="post-entry post-entry-type-page post-entry-3489">
      <div class="entry-content-wrapper clearfix">
         <div class="av-special-heading av-special-heading-h2    avia-builder-el-0  el_before_av_one_half  avia-builder-el-first  " style="padding-bottom:10px;">
            <h2 style="padding-bottom:10px;" itemprop="headline" class="av-special-heading-tag">Create Event Admin</h2>
            <div class="special-heading-border">
               <div class="special-heading-inner-border"></div>
            </div>
         </div>
         <div class="flex_column av_one_half first  avia-builder-el-1  el_after_av_heading  el_before_av_one_half  ">
            <form data-avia-redirect="" data-avia-form-id="1" class="avia_ajax_form  avia-builder-el-2  avia-builder-el-no-sibling " method="post" action="http://mastersoftwaretechnologies.com/table_networking/?page_id=3489">
               <fieldset>
                  <p id="element_avia_admin_name_1" class=" first_form  form_element form_fullwidth"><label for="avia_admin_name_1">Admin Name <abbr title="required" class="required">*</abbr></label> <input type="text" value="" id="avia_admin_name_1" class="text_input is_empty" name="avia_admin_name_1"></p>
                  <p id="element_avia_admin_e-mail_1" class=" first_form  form_element form_fullwidth"><label for="avia_admin_e-mail_1">Admin E-Mail <abbr title="required" class="required">*</abbr></label> <input type="text" value="" id="avia_admin_e-mail_1" class="text_input is_email" name="avia_admin_e-mail_1"></p>
                  <p id="element_avia_admin_password_1" class=" first_form  form_element form_fullwidth"><label for="avia_admin_password_1">Admin Password <abbr title="required" class="required">*</abbr></label> <input type="text" value="" id="avia_admin_password_1" class="text_input is_empty" name="avia_admin_password_1"></p>
                  <p id="element_avia_events_muti-select_1" class=" first_form  form_element form_fullwidth">
                     <label for="avia_events_muti-select_1">Events (Muti-Select) <abbr title="required" class="required">*</abbr></label> 
                     <select id="avia_events_muti-select_1" class="select is_empty" name="avia_events_muti-select_1">
                        <option value="Event 1">Event 1</option>
                        <option value="Event 2">Event 2</option>
                        <option value="Event 3">Event 3</option>
                        <option value="Event 4">Event 4</option>
                        <option value="Event 5">Event 5</option>
                     </select>
                  </p>
                  <p class="hidden"><input type="text" value="" id="avia_avia_username_1" class="hidden " name="avia_avia_username_1"></p>
                  <p class="form_element "><input type="hidden" name="avia_generated_form1" value="1"><input type="submit" data-sending-label="Sending" class="button" value="Create Event"></p>
               </fieldset>
            </form>
            <div class="ajaxresponse ajaxresponse_1 hidden" id="ajaxresponse_1"></div>
         </div>
         <div class="flex_column av_one_half   avia-builder-el-3  el_after_av_one_half  avia-builder-el-last  ">
            <div class="avia-data-table-wrap avia_responsive_table">
               <table itemtype="https://schema.org/Table" itemscope="itemscope" class="avia-table avia-data-table avia-table-1  avia-builder-el-4  el_before_av_codeblock  avia-builder-el-first ">
                  <caption>Boss User Manage Admin</caption>
                  <tbody>
                     <tr class="">
                        <td class="">Name</td>
                        <td class="">Email</td>
                        <td class="">Password</td>
                        <td class="">Associated Events</td>
                     </tr>
                     <tr class="">
                        <td class="">Event Admin1</td>
                        <td class="">admin@event.com</td>
                        <td class="">Admin1</td>
                        <td class=""><a href="#">Test Event</a></td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <style type="text/css"></style>
            <section itemtype="https://schema.org/CreativeWork" itemscope="itemscope" class="avia_codeblock_section avia_code_block_0">
               <div itemprop="text" class="avia_codeblock ">
                  <nav class="pagination">
                     <!--span class="pagination-meta">Page 1 of 6</span-->
                     <span class="current">1</span>
                     <a href="#" class="inactive">2</a>
                     <a href="#" class="inactive">3</a>
                     <a href="#">?</a>
                     <a href="#">�</a>
                  </nav>
               </div>
            </section>
         </div>
      </div>
   </div>
</main>

