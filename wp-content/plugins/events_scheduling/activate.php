<?php
global $wpdb;

$tblconslt1 = "CREATE TABLE IF NOT EXISTS Users (
  id int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(100) NOT NULL,
  Email varchar(100) NOT NULL,
  Password varchar(100) NOT NULL,
  User_type int(11) NOT NULL,
  valid_for_events varchar(200) NOT NULL,
  Registerd_on Datetime NOT NULL,
  acess_level varchar(200) NOT NULL,
  added_by int(11) NOT NULL,
  subscription_fees float NOT NULL,
  Validupto Datetime NOT NULL,
  location varchar(256) NOT NULL,
  u_long text NOT NULL,
  u_lat text NOT NULL,
associated_event varchar(200) NOT NULL,
  PRIMARY KEY (id)
);";

$tblconslt2 = "CREATE TABLE fees(
  id int(11) NOT NULL AUTO_INCREMENT,
  fees varchar(60) NOT NULL,
  fees_title varchar(60) NOT NULL,
  usertype int(11) NOT NULL,
  is_active int(11) NOT NULL,
  PRIMARY KEY (id)
);";

$tblconslt3 = "CREATE TABLE IF NOT EXISTS Events(
  id int(11) NOT NULL AUTO_INCREMENT,
  event_name varchar(200) NOT NULL,
  event_start_date_time datetime NOT NULL,
  no_of_peoples varchar(30) NOT NULL,
  event_type varchar(100) NOT NULL,
  event_fees float NOT NULL,
  event_url varchar(100) NOT NULL,
  more_ifo_url varchar(200) NOT NULL,
  boss_id int(11) NOT NULL,
  event_num_tables int(11) NOT NULL,
  event_num_seats_per_table int(11) NOT NULL,
  event_number_of_guests int(11) NOT NULL,
  event_number_of_rounds int(11),
  PRIMARY KEY (id)
);";
$tblconslt3 = "CREATE TABLE IF NOT EXISTS Events_reg(
  id int(11) NOT NULL AUTO_INCREMENT,
  event_name varchar(200) NOT NULL,
  event_location varchar(200) NOT NULL,
  e_long text NOT NULL,
  e_lat text NOT NULL,
  event_date varchar(30) NOT NULL,
 no_of_people int(11)NOT NULL,
  event_fees varchar(200) NOT NULL,
  event_type varchar(200) NOT NULL,
  expectations varchar(100) NOT NULL,
  duration_of_the_event varchar(200) NOT NULL,
  food_items varchar(256) NOT NULL,
  public_event int(11) NOT NULL,
  late_fees varchar(200) NOT NULL,
  private_event varchar(11) NOT NULL,
  recurring_event varchar(11) NOT NULL,
  run_event int(11) NOT NULL,
payment_status int(11) NOT NULL,
  PRIMARY KEY (id)
);";
$tblconslt4 = "CREATE TABLE IF NOT EXISTS tables (
  id int(11) NOT NULL AUTO_INCREMENT,
  json text NOT NULL,
  status int(11) NOT NULL,
  PRIMARY KEY (id)
);";

$tblconslt5 = "CREATE TABLE IF NOT EXISTS subscriptions (
  id int(11) NOT NULL AUTO_INCREMENT,
  fees float NOT NULL,
  due_date datetime NOT NULL,
  user_id int(11) NOT NULL,
  PRIMARY KEY (id)
);";

$tblconslt6 = "CREATE TABLE IF NOT EXISTS usersinfo (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  sex varchar(100) NOT NULL,
  address varchar(250) NOT NULL,
  image varchar(250) NOT NULL,
  home_town varchar(200) NOT NULL,
  country varchar(100) NOT NULL,
  phone_number varchar(100) NOT NULL,
  PRIMARY KEY (id)
);";

$tblconslt7 = "CREATE TABLE IF NOT EXISTS ratings (
  id int(11) NOT NULL AUTO_INCREMENT,
  rating_date Datetime NOT NULL,
  rating_from int(11) NOT NULL,
  rating_to int(11) NOT NULL,
  score varchar(250) NOT NULL,
  event_id int(11) NOT NULL,
  PRIMARY KEY (id)
);";
$tblconslt8 = "CREATE TABLE IF NOT EXISTS table_logic (
  id int(11) NOT NULL AUTO_INCREMENT,
  round int(11) NOT NULL,
  table int(11) NOT NULL,
  people varchar(256) NOT NULL,
  PRIMARY KEY (id)
);";
$tblconslt9 = "CREATE TABLE IF NOT EXISTS players (

id int(11) NOT NULL AUTO_INCREMENT,
player_id int(11) NOT NULL,
event_id int(11) NOT NULL,
user_id int(11) NOT NULL,
 PRIMARY KEY (id)

);";
$tblconslt11 = "CREATE TABLE IF NOT EXISTS newsletters (

id int(11) NOT NULL AUTO_INCREMENT,
name varchar(256) NOT NULL,
title varchar(256) NOT NULL,
content text NOT NULL,
status int(11) NOT NULL,
 PRIMARY KEY (id)

);";
//$tblconslt8="CREATE TABLE IF NOT EXISTS User_type (
//  id int(11) NOT NULL AUTO_INCREMENT,
//  user_type varchar(250) NOT NULL,
//  PRIMARY KEY (id)
//);";
$consltint  = "INSERT INTO `fees` (
`id` ,
`fees` ,
`fees_title` ,
`usertype` ,
`is_active`
)
VALUES (
NULL , '0', '1', '0', '0'
), (
NULL , '0', '2', '0', '0'
),
(
NULL , '0', '3', '0', '0'
);";
wp_insert_post($pagemodalhome); //for boss sign up/login

//ModelHomes Section
$pagemodalhome = array(
    'post_author' => 'Events',
    'post_content' => '[Boss Sign Up/Login]',
    'post_name' => 'Boss Sign Up/Login',
    'post_status' => 'publish',
    'post_title' => 'Boss Sign Up/Login',
    'post_excerpt' => 'BuilderLinked',
    'post_type' => 'page',
    'comment_status' => 'closed',
    'ping_status' => 'closed',
    'post_parent' => 0,
    'menu_order' => 2,
    'to_ping' => '',
    'pinged' => ''
);
wp_insert_post($pagemodalhome1); //for dashboard

//ModelHomes Section
$pagemodalhome1 = array(
    'post_author' => 'Events',
    'post_content' => '[dashboard_go]',
    'post_name' => 'Dashboard',
    'post_status' => 'publish',
    'post_title' => 'Dashboard',
    'post_excerpt' => 'BuilderLinked',
    'post_type' => 'page',
    'comment_status' => 'closed',
    'ping_status' => 'closed',
    'post_parent' => 0,
    'menu_order' => 2,
    'to_ping' => '',
    'pinged' => ''
);

wp_insert_post($pagemodalhome2); //for dashboard

//ModelHomes Section
$pagemodalhome2 = array(
    'post_author' => 'Events',
    'post_content' => '[dashboard_go]',
    'post_name' => 'Dashboard',
    'post_status' => 'publish',
    'post_title' => 'Dashboard',
    'post_excerpt' => 'BuilderLinked',
    'post_type' => 'page',
    'comment_status' => 'closed',
    'ping_status' => 'closed',
    'post_parent' => 0,
    'menu_order' => 2,
    'to_ping' => '',
    'pinged' => ''
);

wp_insert_post($pagemodalhome2); //for subscriber dashboard dashboard

//ModelHomes Section
$pagemodalhome2 = array(
    'post_author' => 'Events',
    'post_content' => '[events_regi]',
    'post_name' => 'Subscriber Dashboard',
    'post_status' => 'publish',
    'post_title' => 'Subscriber Dashboard',
    'post_excerpt' => 'BuilderLinked',
    'post_type' => 'page',
    'comment_status' => 'closed',
    'ping_status' => 'closed',
    'post_parent' => 0,
    'menu_order' => 2,
    'to_ping' => '',
    'pinged' => ''
);
wp_insert_post($pagemodalhome3); //for subscriber dashboard dashboard

//ModelHomes Section
$pagemodalhome3 = array(
    'post_author' => 'Events',
    'post_content' => '[events-display]',
    'post_name' => 'Events Public Display',
    'post_status' => 'publish',
    'post_title' => 'Events Public Display',
    'post_excerpt' => 'BuilderLinked',
    'post_type' => 'page',
    'comment_status' => 'closed',
    'ping_status' => 'closed',
    'post_parent' => 0,
    'menu_order' => 2,
    'to_ping' => '',
    'pinged' => ''
);
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($tblconslt1);
dbDelta($tblconslt2);
dbDelta($tblconslt3);
dbDelta($tblconslt4);
dbDelta($tblconslt5);
dbDelta($tblconslt6);
dbDelta($tblconslt7);
dbDelta($tblconslt8);
dbDelta($tblconslt9);
dbDelta($tblconslt10);
dbDelta($tblconslt11);
dbDelta($consltint);
?>
