<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'events_project');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', "root");

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N4CYpj|NS(aQTem{7F4}4$:5HRW`L8,t$bYq!%(Lm]1}T>k{q2^5A>[<-#*_re_2');
define('SECURE_AUTH_KEY',  'gM~C%~0<M9*||f6|Ft!CCxcanG4KY/%3p.)Go-(+l_Kr+WX#j@&R|7)lgS0/+Max');
define('LOGGED_IN_KEY',    '5w++}56zwN)JSUT_7-Q{5z-i4`VL.Tf}0G [HCm:{?w{3/kpD{Iq=Q0i$:<1ky@O');
define('NONCE_KEY',        '!VqD/n)/[Gclx*#=||a.LolX5b+?C+>iE8plZo[ws/gd&pfVh<:1<_m =g~g<?JB');
define('AUTH_SALT',        '5F8XiP+*3DR#;J(GvCLNi])K%7.ZrF-3g%t:TtcW99;|ny<86D3? &D.Bt/Xu9:t');
define('SECURE_AUTH_SALT', '2E}maqcApYONJ!(1+2%x@1$5yl0-L1< PJI:;PU*L3hNS34(KM6+M$70kha^2?qh');
define('LOGGED_IN_SALT',   'OJJh6o%I qZTVO?E9t-W;hWxExy)wi:Y_- -qGGHgTFRJ0UN9Y_EX97vy^[jIBfx');
define('NONCE_SALT',       'k|nmTa4L M~MK@?)z#fA0+C)t.&QEp2??t;b,WI-)r[JJy>{=?1-(!Yc_@El[*;O');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
